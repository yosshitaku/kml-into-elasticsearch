import fs from "fs";
import xml2js from "xml2js";
import { Client } from "@elastic/elasticsearch";
import path from "path";

interface Folder {
  name: string[];
  Placemark: {
    name: string[];
    styleUrl: string[];
    Point?: {
      coordinates: string[];
    }[];
    LineString?: {
      tessellate: string[];
      coordinates: string[];
    }[];
  }[];
}

const client = new Client({
  node: "http://localhost:9200",
});

const addData = (filepath: string): Promise<void> => {

  return new Promise((resolve, reject) => {
    const xmlText = fs.readFileSync(filepath, "utf-8");

    xml2js.parseString(xmlText, async (err, result) => {
      if (err) {
        console.log(err.message);
        reject();
      } else {
        // console.log(result.kml.Document[0].Folder);
    
        const locations: {
          name: string;
          frame: number;
          location: { lat: string; lon: string };
        }[] = [];
    
        result.kml.Document[0].Folder.forEach((f: Folder) => {
            console.log(f)
          f.Placemark.forEach((p) => {
            if (p.LineString !== undefined) {
              p.LineString.forEach((ls) => {
                ls.coordinates.forEach((co) => {
                  co.split(/\r|\n/)
                    .filter((co) => co !== "")
                    .forEach((co, index) => {
                      const text = co.trim().split(",");
                      if (text.length > 2) {
                        locations.push({
                          name: f.name[0],
                          frame: index,
                          location: {
                            lon: text[0],
                            lat: text[1],
                          },
                        });
                      }
                    });
                });
              });
            }
          });
        });
    
        let index = 0;
        for (const loc of locations) {
          console.log(loc);
          await client.index({
            index: "locations",
            id: `${index}_${Date.now().toString()}`,
            document: loc,
          });
          index++;
        }

        resolve();
      }
    });  
  });


}

(async () => {
  const files = (await fs.promises.readdir('./dummy-data')).map(f => path.join('./dummy-data', f));
  for (const f of files) {
    await addData(f)
  }
})();