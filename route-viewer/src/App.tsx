import axios from "axios";
import React, { useEffect, useState } from "react";
import Map from "./components/map.component";

const calcMapHeight = () => {
  const headerElement = document.getElementById("header");
  if (headerElement) {
    return window.innerHeight - headerElement.clientHeight;
  }

  return window.innerHeight;
};

function App() {
  const [mapHeight, setMapHeight] = useState<number>(calcMapHeight());

  const onResizeWindow = () => {
    const height = calcMapHeight();
    setMapHeight(height);
  };

  useEffect(() => {
    window.addEventListener("resize", onResizeWindow);
    window.addEventListener("load", onResizeWindow);

    return () => {
      window.removeEventListener("resize", onResizeWindow);
      window.removeEventListener("load", onResizeWindow);
    };
  }, []);

  const fetchRoutes = (args: {
    topLeft: { lat: number; lon: number };
    bottomRight: { lat: number; lon: number };
  }) => {
    const query = {
      size: 10000,
      sort: [
        {
          frame: {
            order: 'asc'
          }
        }
      ],
      query: {
        bool: {
          must: {
            match_all: {},
          },
          filter: {
            geo_bounding_box: {
              location: {
                top_left: args.topLeft,
                bottom_right: args.bottomRight,
              },
            },
          },
        },
      },
      aggs: {
        routes: {
          terms: {
            field: "name.keyword",
            size: 10,
          },
        },
      },
    };

    console.log(query);

    axios
      .get("http://localhost:9200/locations/_search", {
        params: {
          source: JSON.stringify(query),
          source_content_type: "application/json",
        },
      })
      .then((data) => {
        console.log(data);
      });
  };

  return (
    <Map
      height={mapHeight}
      onMoveEnd={(map) => {
        const bound = map.getBounds();
        fetchRoutes({
          topLeft: {
            lat: bound.getNorthWest().lat,
            lon: bound.getNorthWest().lng,
          },
          bottomRight: {
            lat: bound.getSouthEast().lat,
            lon: bound.getSouthEast().lng,
          },
        });
      }}
    />
  );
}

export default App;
