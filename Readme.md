## Tips

### About Virtual Memory

* https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

> To set this value permanently, update the vm.max_map_count setting in /etc/sysctl.conf. To verify after rebooting, run sysctl vm.max_map_count.

```bash
sysctl -w vm.max_map_count=262144
```

WSL .wslconfig on your windows host pc.

```
[wsl2]
kernelCommandLine = "sysctl.vm.max_map_count=262144"
```

### 位置情報の検索について

* https://dev.classmethod.jp/articles/study-elasticsearch-geolocation/

### Mapping

```
PUT locations
{
  "settings": {
    "analysis": {
      "analyzer": {
        "my_ja_analyzer": {
          "type": "custom",
          "char_filter":[
                "icu_normalizer"
          ],
          "tokenizer": "kuromoji_tokenizer",
          "filter": [
            "kuromoji_baseform",
            "kuromoji_part_of_speech",
            "ja_stop",
            "kuromoji_number",
            "kuromoji_stemmer"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "name": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword"
          },
          "ja": {
            "type": "text",
            "analyzer": "my_ja_analyzer",
            "fielddata": true
          }
        }
      },
      "frame": {
        "type": "integer"
      },
      "location": {
        "type": "geo_point"
      }
    }
  }
}
```