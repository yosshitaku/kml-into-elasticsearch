import React, { useCallback, useEffect, useState } from "react";
import { Map as LMap, tileLayer } from "leaflet";
import styled from "styled-components";

import '../../node_modules/leaflet/dist/leaflet.css';

const MapElement = styled.div<{ height: number | null }>`
  height: ${(props) => (props.height ? props.height + "px" : "")};
  width: 100%;
`;

type Props = {
  height: number | null;
  onMoveEnd: (map: LMap, event?: L.LeafletEvent) => void;
};

const Map: React.FC<Props> = ({ height, onMoveEnd }) => {
  const [mapInstance, setMapInstance] = useState<LMap | null>(null);
  const mapRef = useCallback((node: HTMLDivElement | null) => {
    if (node !== null && mapInstance === null) {
      const initializedMap = new LMap(node).setView(
        [35.629044267158456, 139.73937913744058],
        16
      );

      tileLayer("https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}", {
        attribution:
          "<a href='https://developers.google.com/maps/documentation' target='_blank'>Google Map</a>",
      }).addTo(initializedMap);

      initializedMap.addEventListener('moveend', (event) => {
        onMoveEnd(initializedMap, event);
      })

      initializedMap.invalidateSize();
      setMapInstance(initializedMap);
    }
  }, []);

  useEffect(() => {
    if (mapInstance) {
      mapInstance.invalidateSize();
    }
  }, [mapInstance]);

  if (height === null) {
    return null;
  }

  return <MapElement height={height} ref={mapRef} />;
};

export default Map;
